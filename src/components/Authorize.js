import { Fragment } from "react";
import useAuthorize from "../hooks/useAuthorize";

function Authorize({
  to = "/login",
  forbiddenRedirectTo = "/",
  roles = ["MEMBER"],
  children,
}) {
  useAuthorize({
    forbiddenRedirectURL: forbiddenRedirectTo,
    unauthorizedRedirectionURL: to,
    whiteListedRoles: roles,
  });

  return <Fragment>{children}</Fragment>;
}

export default Authorize;
