import "./Task.css";

function Task(props) {
  const {
    id,
    content,
    isCompleted,
    completedAt,
    dueAt,
    onCheck = () => {},
  } = props;
  const style = {
    textDecoration: isCompleted ? "line-through" : "unset",
  };

  return (
    <li className="Task-container" style={style}>
      <div>
        <p>{content}</p>
        {!!dueAt && <p>{dueAt}</p>}
      </div>
      <input
        type="checkbox"
        checked={!!isCompleted}
        onChange={() => onCheck({ ...props, onCheck: undefined })}
      />
    </li>
  );
}

export default Task;
