import { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { getRoles } from "../services/authService";
import AuthContext from "../context/AuthContext";

export default function useAuthorize({
  whiteListedRoles = ["MEMBER", "MITRA"],
  forbiddenRedirectURL = "/",
  unauthorizedRedirectionURL = "/login",
}) {
  const user = useContext(AuthContext);
  const [roles, setRoles] = useState([]);
  const navigate = useNavigate();

  // On user changed
  useEffect(() => {
    async function effect() {
      const isRolesEmpty = roles.length <= 0;

      if (!user) {
        setRoles([]);
        navigate(unauthorizedRedirectionURL, {
          replace: true,
        });
      }

      if (isRolesEmpty) {
        const rolesResponse = getRoles();
        await setRoles(rolesResponse.data.roles.map((i) => i.name));
      }

      // If roles is not on the whiteListedRoles
      // Then redirect
      if (!isRolesEmpty && !roles.some((i) => whiteListedRoles.includes(i))) {
        navigate(forbiddenRedirectURL, { replace: true });
      }
    }

    effect();
  }, [
    user,
    roles,
    forbiddenRedirectURL,
    navigate,
    whiteListedRoles,
    unauthorizedRedirectionURL,
  ]);

  return user;
}
