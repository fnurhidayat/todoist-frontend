import classNames from "classnames";

function MainLayout({ children, className }) {
  return (
    <div className="App">
      <header className={classNames("App-header", className)}>
        {children}
      </header>
    </div>
  );
}

export default MainLayout;
