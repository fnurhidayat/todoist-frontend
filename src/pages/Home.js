import { useEffect, useState, useContext } from "react";
import MainLayout from "../layouts/main";
import AuthContext from "../context/AuthContext";
import { logout } from "../services/authService";
import { getTasks, putTask } from "../services/taskService";
import Task from "../components/Task";
import Authorize from "../components/Authorize";
import "./Home.css";

function Home() {
  const [isLoading, setLoading] = useState(false);
  const [tasks, setTasks] = useState([]);
  const user = useContext(AuthContext);

  // On mount and user changed
  useEffect(() => {
    loadTasks();
  }, []);

  function onLogoutClick() {
    return logout();
  }

  function loadTasks() {
    setLoading(true);

    // Get task from backend
    getTasks()
      .then((res) => {
        setTasks(res.data.data?.tasks);
      })
      .catch(console.log)
      .finally(() => setLoading(false));
  }

  function onTaskMarked({ id, ...payload }) {
    putTask(id, {
      ...payload,
      isCompleted: !payload.isCompleted,
    })
      .then(loadTasks)
      .catch(console.log);
  }

  return (
    <Authorize>
      <MainLayout>
        <div className="Home-container">
          <h1>Todoist</h1>
          <p>Hello, {user?.email?.split("@")[0]}!</p>
          <button className="Home-logoutButton" onClick={onLogoutClick}>
            Logout
          </button>

          {isLoading ? (
            <p>Loading...</p>
          ) : (
            <ul className="Task-list">
              {tasks.map((task) => (
                <Task key={task.id} {...task} onCheck={onTaskMarked} />
              ))}
            </ul>
          )}
        </div>
      </MainLayout>
    </Authorize>
  );
}

export default Home;
