import { useContext } from "react";
import MainLayout from "../layouts/main";
import { logout } from "../services/authService";
import AuthContext from "../context/AuthContext";
import Authorize from "../components/Authorize";
import "./Home.css";

function KhususMitra() {
  const user = useContext(AuthContext);

  function onLogoutClick() {
    return logout();
  }

  return (
    <Authorize roles={["MITRA"]}>
      <MainLayout>
        <div className="Home-container">
          <h1>Mitra</h1>
          <p>Hello, {user?.email?.split("@")[0]}!</p>
          <button className="Home-logoutButton" onClick={onLogoutClick}>
            Logout
          </button>
        </div>
      </MainLayout>
    </Authorize>
  );
}

export default KhususMitra;
