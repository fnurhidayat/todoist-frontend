import MainLayout from "../layouts/main";
import RegisterForm from "../components/RegisterForm";
import { register } from "../services/authService";
import useLocalStorage from "../hooks/useLocalStorage";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { ACCESS_TOKEN } from "../config/localStorage";
import "./Register.css";

function Register() {
  const [accessToken, setAccessToken] = useLocalStorage(ACCESS_TOKEN, null);
  const navigate = useNavigate();

  function onSubmit({ email, password }) {
    register(email, password)
      .then((credential) => {
        setAccessToken(credential?.user?.accessToken);
        navigate("/", { replace: true });
      })
      .catch(console.log);
  }

  return (
    <MainLayout className="Register-container">
      <h1>Register</h1>
      <p>Want to be Todoist?</p>

      <div>
        <RegisterForm onSubmit={onSubmit} />
      </div>
    </MainLayout>
  );
}

export default Register;
