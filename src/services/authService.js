import {
  getAuth,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  onAuthStateChanged,
  signOut,
} from "firebase/auth";
import firebase from "./firebaseService";

const auth = getAuth(firebase);

export function register(email, password) {
  return createUserWithEmailAndPassword(auth, email, password);
}

export function login(email, password) {
  return signInWithEmailAndPassword(auth, email, password);
}

export function onUserChanged(callback) {
  return onAuthStateChanged(auth, callback);
}

export function logout() {
  return signOut(auth);
}

export async function getRoles() {
  // Implement real request to your backend.
  return {
    status: "OK",
    data: {
      roles: [
        {
          id: 1,
          name: "MEMBER",
        },
      ],
    },
  };
}

export const currentUser = auth.currentUser;
